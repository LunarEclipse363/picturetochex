# Changelog

# v1.1.0
- Added support for PCD8544 horizontal mode output (`--mode` flag)
- Added support for manually setting the name of the output array (`-n`/`--name` flag)
- Changed the default name of the output array to `bitmap_for_display`
- Removed automatic naming of the output array based on the input file
- Added a `-O` shorthand for the `--overwrite` flag

# v1.0.0
- Initial Release
- Support for PCD8544 vertical mode output
- Support for inverting the colors of the output (`-i`/`--invert` flag)
- Support for outputting to stdout or files (`-o`/`--output` flag)
- Overwriting the output file has to be explicitly allowed (`--overwrite` flag)
