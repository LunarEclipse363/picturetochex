# PictureToCHex
This script was written specifically for use with Nokia 5110 (PCD8544) screens, but it should be usable for other monochromatic displays.
It outputs an array you can import or paste into a C program and then just send the bytes sequentially to the display (in vertical mode).

## Requirements
You should prepare a black-and-white picture for this, although the program will attempt to binarize it.

This program supports Python 3.10 and later.
The required libraries are Pillow and Numpy.

## Usage
```
usage: picturetochex [-h] [--version] [-i] [-m {vert,hori}] [-n NAME] [-o OUTPUT] [-O] FILENAME

A program to convert images into a C byte array.

positional arguments:
  FILENAME              the image to be converted

options:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -i, --invert          inverts the resulting binary values of the pixels
  -m {vert,hori}, --mode {vert,hori}
                        equivalent to modes available in PCD8544
  -n NAME, --name NAME  name of the output array (default: 'bitmap_for_display')
  -o OUTPUT, --output OUTPUT
                        save the output to a file instead of printing to stdout
  -O, --overwrite       overwrite the output file if exists

This program is released under the AGPL-3.0 license. Copyright 2023 LunarEclipse <luna@lunareclipse.zone>
```

## Example
TODO
<!--TODO: add a blog post about this -->

## License
This program is released under the [AGPL-3.0 license](https://www.gnu.org/licenses/agpl-3.0.html). Copyright 2023 LunarEclipse.
