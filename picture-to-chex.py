#!/usr/bin/env python3

import sys
import argparse
from PIL import Image
import numpy

parser = argparse.ArgumentParser(
        prog="picturetochex",
        description="A program to convert images into a C byte array.",
        epilog="This program is released under the AGPL-3.0 license. Copyright 2023 LunarEclipse <luna@lunareclipse.zone>")
parser.add_argument('file', metavar='FILENAME', type=argparse.FileType('rb'), help="the input image")
parser.add_argument('--version', action='version', version='%(prog)s 1.1.0')
parser.add_argument('-i', '--invert', action='store_true', help="inverts the resulting binary values of the pixels")
parser.add_argument('-m', '--mode', default='vert', choices=['vert', 'hori'], help="equivalent to modes available in PCD8544")
parser.add_argument('-n', '--name', default="bitmap_for_display", help="name of the output array (default: 'bitmap_for_display')")
parser.add_argument('-o', '--output', help="save the output to a file instead of printing to stdout")
parser.add_argument('-O', '--overwrite', action='store_true', help="overwrite the output file if exists")

args = parser.parse_args()

img = Image.open(args.file)

width = img.size[0]
height = img.size[1]

if (height % 8) != 0:
    print("Height not divisible by 8")
    sys.exit(1)

img = img.convert("1") # convert to black/white

rawvalues = list()

for x in range(width):
    for y in range(height):
        rawvalues.append(img.getpixel((x, y)))

hexvalues = numpy.packbits(list(map(lambda x : (x == 0) ^ args.invert, rawvalues)), bitorder='little')

match args.mode:
    case "vert":
        output = f"const uint8_t {args.name} [] = " + '{\n'
        for col in numpy.split(hexvalues, width):
            output += '    '
            for v in col:
                output += f"0x{v:02x}, "
            output += '\n'
        output += '};'
    case "hori":
        output = f"const uint8_t {args.name} [] = " + '{\n'
        for col in numpy.transpose(numpy.split(hexvalues, width)):
            output += '    '
            for v in col:
                output += f"0x{v:02x}, "
            output += '\n'
        output += '};'
    case _:
        raise Exception("Internal Error: invalid --mode argument value")

# Remove trailing spaces at the ends of lines from the output
output = output.replace(' \n', '\n')
output = output.replace(' \r', '\r')

if args.output is None:
    print(output)
else:
    try:
        with open(args.output, "w" if args.overwrite else "x", encoding="UTF-8") as file:
            file.write(output)
    except FileExistsError as e:
        parser.error(str(e) + '. Use the "--overwrite" flag to prevent this message.')
    except BaseException as e:
        parser.error(str(e))
